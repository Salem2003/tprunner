using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    public float velocidadRotacionY = 0.0f;
    public float velocidadRotacionZ = 0.0f;

    void Update()
    {
        transform.Rotate(0, velocidadRotacionY * Time.deltaTime, velocidadRotacionZ * Time.deltaTime); //roto al obj
    }
}
