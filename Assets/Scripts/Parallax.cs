﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    public float num;
    public ControlCamara inversion;
    public Controller_Player ipj;
    public Controller_Player pj;
    private SpriteRenderer color;
    public int R = 0;
    public int G = 0;
    public int B = 0;

    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        color = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (!inversion.invertido && !pj.muerto)
        {
            color.color = new Color(255, 255, 255); //cambio el color de la img

            transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z); //muevo la img
            if (transform.localPosition.x < -20)
            {
                transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z); //coloco la img al inicio para crear el efecto de parallax
            }
        }
        else if (inversion.invertido && !ipj.muerto)
        {
            color.color = new Color(R, G, B);

            transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
            if (transform.localPosition.x > 20)
            {
                transform.localPosition = new Vector3(-20, transform.localPosition.y, transform.localPosition.z);
            }
        }
    }
}
