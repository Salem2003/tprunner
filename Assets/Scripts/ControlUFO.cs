using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlUFO : MonoBehaviour
{
    public float maxY = 0.0f;
    public float midY = 0.0f;
    public float minY = 0.0f;
    public bool subir = true;
    private int linea = 1;
    private bool entrar = true;
    private float x = 0.0f;

    void Update()
    {
        /*if (transform.localPosition.y <= maxY && subir)
        {
            transform.localPosition = new Vector3(0, 10, 0) * Time.deltaTime;

            if (transform.localPosition.y >= maxY)
            {
                subir = false;
            }
        }
        else if (transform.localPosition.y >= minY && !subir)
        {
            transform.localPosition = new Vector3(0, -10, 0) * Time.deltaTime;

            if (transform.localPosition.y <= minY)
            {
                subir = true;
            }
        }*/

        x = transform.position.x; //guardo la pos x

        if (entrar)
        {
            entrar = false;
            StartCoroutine(CambioLinea(1));
        }
    }

    private IEnumerator CambioLinea(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo--;
        }

        if (tiempo <= 0)
        {
            //muevo al obj entre las lineas 1 y 3
            if (subir)
            {
                linea++;

                if (linea == 2)
                {
                    transform.position = new Vector3(x, maxY, 0);
                    subir = false;
                }
            }
            else if (!subir)
            {
                linea--;

                if (linea == 1)
                {
                    transform.position = new Vector3(x, minY, 0);
                    subir = true;
                }
            }

            entrar = true;
        }
    }
}
