using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPared : MonoBehaviour
{
    public GameObject pj1;
    public GameObject pj2;
    //public Controller_Hud perder;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Vortex"))
        {
            Destroy(other.gameObject);
            Destroy(pj1);
            Destroy(pj2);
            Controller_Hud.gameOver = true;
        }
    }
}
