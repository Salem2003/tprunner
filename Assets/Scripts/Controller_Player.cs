﻿using System;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored = true;
    private bool cayendo = false;
    public ControlCamara inversion;
    public bool muerto = false;
    public float posicionLinea1 = 0.0f;
    public float posicionLinea2 = 0.0f;
    public float posicionLinea3 = 0.0f;
    public int pos = 1;
    public float posY1 = 0;
    public float posY2 = 0;
    public float posY3 = 0;
    private Controller_Hud puntos;
    private Collider caja;
    public bool cambiarCollider = false;
    private bool entrar = true;
    private Controller_Instantiator star;

    private void Start()
    {
        /*rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        transform.position = new Vector3(0, -0.5f, 0);*/
        puntos = FindObjectOfType<Controller_Hud>();
        star = FindObjectOfType<Controller_Instantiator>();
        caja = GetComponent<BoxCollider>();
    }

    void Update()
    {
        GetInput();

        /*if (inversion.invertido && entrar)
        {
            caja.enabled = cambiarCollider;
            cambiarCollider = !cambiarCollider;
            entrar = true;
        }
        else if (!inversion.invertido && !entrar)
        {
            caja.enabled = cambiarCollider;
            cambiarCollider = !cambiarCollider;
            entrar = false;
        }*/

    }

    private void GetInput() //leo la tecla presionada x el jugador
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            transform.position = new Vector3(0, posY1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            transform.position = new Vector3(0, posY2, 0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            transform.position = new Vector3(0, posY3, 0);
        }

        /*Jump();
        Caer();
        Duck();*/
    }

    /*private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }*/

    /*private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    transform.position = new Vector3(0, -0.5f, 0);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
                cayendo = true;
            }
        }
    }*/

    /*public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            muerto = true;
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            cayendo = false;
            floored = true;
        }

        if (collision.gameObject.CompareTag("Portal"))
        {
            Destroy(collision.gameObject);
            inversion.invertido = true;
        }
    }*/

    /*private void Caer()
    {
        if (!floored && (transform.position.y >= 5 || cayendo))
        {
            rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
        }
    }*/

    private void OnTriggerEnter(Collider other) //dependiendo del objeto q colisiona con el player, realizo x accion
    {
        if (other.gameObject.CompareTag("NNote") == true)
        {
            Destroy (other.gameObject);
            puntos.puntaje += 2;
        }
        else if (other.gameObject.CompareTag("Enemy"))
        {
            muerto = true;
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }
        else if (other.gameObject.CompareTag("HNote") == true)
        {
            Destroy(other.gameObject);
            puntos.puntaje += 10;
        }
        else if (other.gameObject.CompareTag("Vortex"))
        {
            Destroy(other.gameObject);
            star.powerup = false;
            puntos.puntaje += 200;
            inversion.invertido = !inversion.invertido;
        }
        else if (other.gameObject.CompareTag("Star"))
        {
            Destroy(other.gameObject);
            star.powerup = true;
        }
        else if (other.gameObject.CompareTag("Gold"))
        {
            Destroy(other.gameObject);
            puntos.puntaje += 5;
        }
        else if (other.gameObject.CompareTag("BGold"))
        {
            Destroy(other.gameObject);
            puntos.puntaje += 20;
        }
    }
}
