﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> collectibles;
    public List<GameObject> special;
    public List<GameObject> gold;
    public GameObject bala;
    //public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;
    //public ControlCamara inversion;
    //public GameObject portal;
    public List<GameObject> posiciones;
    //public GameObject NNote;
    private int num = 0;
    private int[] posLimite = { 2, 4 };
    private int[] posColeccionables = { 1, 3, 5 };
    public List<GameObject> objs;
    public bool powerup = false;
    private bool entrar = true;

    void Start()
    {
        //Controller_Enemy.enemyVelocity = 2;
        Movimiento.velocidad = 2; //asigno la velocidad de los objetos spawneables
    }

    void Update()
    {
        //if (!inversion.invertido)
        //{
            SpawnEnemies();
            //ChangeVelocity();
        //}

        if (powerup && entrar)
        {
            entrar = false;
            StartCoroutine(TiempoPowerUp(10));
        }
    }

    /*private void ChangeVelocity()
    {
        time += Time.deltaTime;
        //Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
        Movimiento.velocidad = Mathf.SmoothStep(1f, 15f, time / 45f);
        //portal.GetComponent<Controller_Enemy>().enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }*/

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime; //tiempo restante para instanciar un obj

        if (respawningTimer <= 0)
        {
            num = Random.Range(0, 11);
            //dependiendo del num instancio x obj
            if (!powerup)
            {
                if (num >= 0 && num == 3)
                {
                    if (num == 0 || num == 1)
                    {
                        objs.Add(Instantiate(enemies[0], transform.GetChild(posColeccionables[Random.Range(0, posColeccionables.Length)]).transform));
                    }
                    else
                    {
                        objs.Add(Instantiate(enemies[1], transform.GetChild(posLimite[0]).transform));
                    }
                }
                else if (num >= 4 && num <= 9)
                {
                    objs.Add(Instantiate(collectibles[UnityEngine.Random.Range(0, collectibles.Count)], transform.GetChild(posLimite[Random.Range(0, posLimite.Length)]).transform));
                }
                else if (num == 10 && !powerup)
                {

                    objs.Add(Instantiate(special[UnityEngine.Random.Range(0, special.Count)], transform.GetChild(posLimite[Random.Range(0, posLimite.Length)]).transform));
                    /*objs.Add(Instantiate(special[UnityEngine.Random.Range(0, special.Count)], transform.GetChild(posLimite[1]).transform));
                    objs.Add(Instantiate(special[UnityEngine.Random.Range(0, special.Count)], transform.GetChild(posLimite[2]).transform));
                    objs.Add(Instantiate(special[UnityEngine.Random.Range(0, special.Count)], transform.GetChild(posLimite[3]).transform));
                    objs.Add(Instantiate(special[UnityEngine.Random.Range(0, special.Count)], transform.GetChild(posLimite[4]).transform));*/
                }

                if (num > 4 && num < 9)
                {
                    num = Random.Range(1, 3);
                    InstanciarBala(num);
                }

                respawningTimer = UnityEngine.Random.Range(1, 4);
            }
            else if (powerup)
            {
                objs.Add(Instantiate(gold[UnityEngine.Random.Range(0, gold.Count)], transform.GetChild(posLimite[Random.Range(0, posLimite.Length)]).transform));
                num = Random.Range(1, 3);
                InstanciarBala(num);
                respawningTimer = 1;
            }
            
        }
    }

    private void InstanciarBala(int num)
    {
        num--;
        Instantiate(bala, transform.GetChild(posLimite[Random.Range(0, posLimite.Length)]).transform); //instancia 1-2 balas en cualquiera de las lineas

        if (num > 0)
        {
            InstanciarBala(num); //vuelvo a llamar la funcion si todavia quedan balas x instanciar
        }
    }

    private IEnumerator TiempoPowerUp(float tiempo) //el tiempo q dura el powerup
    {
        while(tiempo > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo--;
        }

        if (tiempo <= 0)
        {
            entrar = true;
            powerup = false;
        }
    }
}
