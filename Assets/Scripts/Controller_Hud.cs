﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public Text pointsText;
    private float distance = 0;
    public int puntaje = 0;

    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameOver)
        {
            //limpio los textos de distancia y puntaje, y muestro el de game over
            distanceText.text = "";
            pointsText.text = "";
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString() + "\n Total Score: " + puntaje.ToString();
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            //modifico la distancia y la muestro junto con el puntaje en pantalla
            distance = Time.time;
            distance += Time.deltaTime;
            distance = (int)distance;
            distanceText.text = "Distance: " + distance.ToString();
            pointsText.text = "Score: " + puntaje.ToString();
        }
    }
}
