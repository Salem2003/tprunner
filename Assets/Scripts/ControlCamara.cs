using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public bool invertido = false;
    public GameObject pl1;
    public GameObject pl2;
    public GameObject fl1;
    public GameObject fl2;
    public GameObject in1;
    public GameObject in2;
    public GameObject par1;
    public GameObject par2;
    private int i = 0;

    void Update()
    {
        //invierto la camara, y cambio los objs, el piso y el color de la img del parallax
        if (invertido)
        {
            pl1.gameObject.GetComponent<BoxCollider>().enabled = false; //desactivo el collider
            fl1.gameObject.SetActive(false);
            in1.gameObject.SetActive(false);
            pl2.gameObject.GetComponent<BoxCollider>().enabled = true;
            fl2.gameObject.SetActive(true);
            in2.gameObject.SetActive(true);
            //par1.gameObject.SetActive(false);
            //par2.gameObject.SetActive(true);

            if (i < (in1.GetComponent<Controller_Instantiator>().objs.Count - 1))
            {
                Destroy(in1.GetComponent<Controller_Instantiator>().objs[i]); //destruyo los objs del lado contrario
                i++;
            }

            //if (transform.position.y > -13)
            //{
                transform.position = new Vector3(4, -13, -10);
            //}

            //if (transform.rotation.z > -180)
            //{
                transform.rotation = Quaternion.Euler(0, 0, -180);
            //}
        }
        else if (!invertido)
        {
            pl1.gameObject.GetComponent<BoxCollider>().enabled = true; ;
            fl1.gameObject.SetActive(true);
            in1.gameObject.SetActive(true);
            pl2.gameObject.GetComponent<BoxCollider>().enabled = false;
            fl2.gameObject.SetActive(false);
            in2.gameObject.SetActive(false);
            //par1.gameObject.SetActive(true);
            //par2.gameObject.SetActive(false);

            if (i < (in2.GetComponent<Controller_Instantiator>().objs.Count - 1))
            {
                Destroy(in2.GetComponent<Controller_Instantiator>().objs[i]);
                i++;
            }

            //if (transform.position.y < 1)
            //{
            transform.position = new Vector3(4, 1, -10);
            //}

            //if (transform.rotation.z < 0)
            //{
            transform.rotation = Quaternion.Euler(0, 0, 0);
            //}
        }
    }
}
